import React, { useState,useEffect ,useRef} from "react";
import GoogleMapReact from "google-map-react";
import '../assets/scss/contact.css'
import Header from '../components/Header'
import HambergaerMenu from './HambergaerMenu'
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import $ from 'jquery';
const Contact = () => {
  const [hamshow , setHamshow ] = useState(false)
  let textRef = useRef('')
  const [name , setName ] = useState('')
  const [email , setEmail ] = useState('')
  const [detail , setDetail ] = useState('')
  useEffect(()=>{
    if(hamshow){
      function name() {
        $('.contact__cotainer__scroll').addClass('contact__cotainer__scroll2').removeClass('contact__cotainer__scroll');
      }
      name()
    }else{
      function name() {
        $('.contact__cotainer__scroll2').addClass('contact__cotainer__scroll').removeClass('contact__cotainer__scroll2');
      }
      name()
    }
  },[hamshow])
  const useStyles = makeStyles({
    root: {
      margin: "0 auto" ,
      fontFamily: 'sarabun',
      marginBottom:10,
      color: "black",
      height:50,
      width:'60%',
      ['@media (max-width:520px)']: { // eslint-disable-line no-useless-computed-key
        width: '90%',
        fontSize:'3vmin'
      },
      fontSize:"1.5vmin",
      borderRadius:25,
      "&.Mui-focused": {
        border: "1px solid #27aa17",
        '& .MuiOutlinedInput-notchedOutline': {
          border: 'none'
        }
      },
    },
    root2: {
      paddingTop:0,
      margin: "0 auto" ,
      fontFamily:'sarabun',
      color: "black",
      height:170,
      ['@media (max-width:520px)']: { // eslint-disable-line no-useless-computed-key
        width: '90%',
        fontSize:'3vmin'
      },
      width:'60%',
      
      fontSize:"1.5vmin",
      borderRadius:25,
      "&.Mui-focused": {
        border: "2px solid #27aa17",
        '& .MuiOutlinedInput-notchedOutline': {
          border: 'none'
        }
      },
    },
    
  });
  
  const classes = useStyles();
  const props = {
    center: {
      lat: 13.895592228709159,
      lng: 100.65680773264573,
    },
    zoom: 16,
  };
  const renderMarkers = (map, maps) => {
    let marker = new maps.Marker({
      position: { lat: 13.895592228709159, lng: 100.65680773264573 },
      map,
      title: "zenalyse",
    });
    return marker;
  };

  const StyledButton = withStyles({
    root: {
      margin: "0 auto" ,
      display:"block",
      background: "#27aa17",
      borderRadius: 24,
      border: 0,
      color: 'white',
      height: 48,
      padding: '0 50px',
      boxShadow: '0 3px 5px 2px rgba(5, 105, 135, .3)',
      fontSize:'1.5vmin',
      marginTop:20,
      ['@media (max-width:520px)']: { // eslint-disable-line no-useless-computed-key
        fontSize:'3vmin',
        marginBottom:40
      },
    },
    label: {
      textTransform: 'capitalize',
    },
  })(Button);
  return (
    <div className="contact__main_container">
        <div className="contact_container__header" >
            <Header />
            <HambergaerMenu togglehamberger={setHamshow}/>
        </div>
        <div className="contact__cotainer__scroll">
            <div className="googlemap">
            <GoogleMapReact
              bootstrapURLKeys={{ key: "AIzaSyAxPiV7f9KXOE9P49a8OSEjcFuZRRfGY7I" }}
              defaultCenter={props.center}
              defaultZoom={props.zoom}
              onGoogleApiLoaded={({ map, maps }) => renderMarkers(map, maps)}
            >
            </GoogleMapReact>
          </div>
          <div className="contact__boxMessage">
            <h1 >Zenalyse Company Limited</h1>
            <p>เวลาทำการ วันจันทร์ – วันศุกร์ 08:30 – 17:30 น.</p>
          </div>
          <div className="container__grid">
            <div className="section1">
              <h1>สำนักงาน</h1>
              <p>555/94 B-Avenue ถ.สุขาภิบาล5 แขวงออเงิน เขตสายไหม กรุงเทพมหานคร 10220</p>
            </div>
            <div className="section2">
              <h1>อีเมล</h1>
              <p>sales@zenalyse.co.th</p>
            </div>
            <div className="section3">
              <h1>ศูนย์บริการลูกค้า</h1>
              <p>02-1587338-9</p>
            </div>
          </div>

          <div className="section4__contact">
              <h1 >ส่งข้อความหาเราทันที</h1>
                <form className={classes.root}  noValidate autoComplete="off">
                <TextField id="outlined-basic"  InputProps={{ className: classes.root }} placeholder="ชื่อ-นามสุกล" variant="outlined" onChange={(e)=>setName(e.target.value)}/><br/>
                <TextField id="outlined-basic" InputProps={{ className: classes.root }} placeholder="อีเมลล์" variant="outlined" onChange={(e)=>setEmail(e.target.value)}/><br/>
                <TextField
                    variant="outlined" InputProps={{ className: classes.root2 }}
                    placeholder="ข้อเสนอแนะ"
                    multiline
                    rows={6}
                    variant="outlined"
                    onChange={(e)=>setDetail(e.target.value)}
                  />
                  <StyledButton ><a style={{textDecoration:'none',color:'white'}} 
                   href={`mailto:sales@zenalyse.co.th?subject=ติดต่อ Zenalyse&body=Hi,${detail} by ${email}`}
                   >Sent Message</a></StyledButton>
                </form>
            </div>
        </div>
    </div>
  );
};

export default Contact;

