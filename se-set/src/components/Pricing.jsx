import React, { useState ,useEffect} from 'react'
import '../assets/scss/pricing.css' 
import HambergaerMenu from './HambergaerMenu'
import Header from './Header'
import {Link} from "react-router-dom";
import $ from 'jquery';
const Pricing = () => {
    const [hamshow , setHamshow ] = useState(false)
    useEffect(()=>{
        if(hamshow){
            function name() {
            $('.pricing__cotainer__scroll').addClass('pricing__cotainer__scroll2').removeClass('pricing__cotainer__scroll');
            }
            name()
        }else{
            function name() {
            $('.pricing__cotainer__scroll2').addClass('pricing__cotainer__scroll').removeClass('pricing__cotainer__scroll2');
            }
            name()
        }
    },[hamshow])
    const data2 = [
        {id:1,title:"การติดตั้ง",status1:false,status2:false,status3:false,active:true},
        {id:2,title:"Local server / Cloud server",status1:true,status2:true,status3:true},
        {id:3,title:"การติดตั้งอุปกรณ์ Hardware",status1:true,status2:true,status3:true},
        {id:4,title:"License การใช้งานและบริการ",status1:false,status2:false,status3:false,active:true},
        {id:5,title:"Site survey",status1:true,status2:true,status3:true},
        {id:6,title:"Software licenses",status1:true,status2:true,status3:true},
        {id:7,title:"Software implementation",status1:true,status2:true,status3:true},
        {id:8,title:"Software training",status1:true,status2:true,status3:true},
        {id:9,title:"Hardware",status1:false,status2:false,status3:false,active:true},
        {id:10,title:"Transmitter",status1:true,status2:true,status3:true},
        {id:11,title:"Signal tower with 3 LED lights (green, yellow, red)",status1:true,status2:true,status3:true},
        {id:12,title:"Station receiver",status1:true,status2:true,status3:true},
        {id:13,title:"Unit control box",status1:true,status2:true,status3:true},
        {id:14,title:"Cable wiring",status1:true,status2:true,status3:true},
        {id:15,title:"Accessories",status1:true,status2:true,status3:true},
        {id:16,title:"ฟีเจอร์พื้นฐาน",status1:false,status2:false,status3:false,active:true},
        {id:17,title:"Machine monitoring",status1:false,status2:true,status3:true},
        {id:18,title:"Time-line",status1:true,status2:true,status3:true},
        {id:19,title:"รายการสถานะเครื่องจักร",status1:true,status2:true,status3:true},
        {id:20,title:"ผลการผลิตแบบ Real-Time",status1:true,status2:true,status3:true},
        {id:21,title:"รายการความคืบหน้าการผลิต",status1:false,status2:false,status3:true},
        {id:22,title:"สรุป OEE",status1:false,status2:false,status3:true},
        {id:23,title:"ฟีเจอร์สนับสนุนการวิเคราะห์",status1:false,status2:false,status3:false,active:true},
        {id:24,title:"กราฟแสดงผลสถานะ",status1:true,status2:true,status3:true},
        {id:25,title:"กราฟแสดงประสิทธิภาพการผลิตของเครื่องจักร",status1:true,status2:true,status3:true},
        {id:26,title:"กราฟแสดงการสูญเสียเวลาในการผลิต",status1:true,status2:true,status3:true},
        {id:27,title:"กราฟแสดงยอดการผลิต",status1:false,status2:false,status3:true},
        {id:28,title:"รายงานสถานะของเครื่องจักร",status1:false,status2:true,status3:true},
        {id:29,title:"รายงานความสูญเสียเวลา",status1:false,status2:true,status3:true},
        {id:30,title:"รายงานผลการผลิต",status1:false,status2:false,status3:true},
        {id:31,title:"ผลสรุปภาพรวม",status1:false,status2:false,status3:true},
        {id:32,title:"การนำเข้าแผนการผลิต",status1:false,status2:false,status3:true},
        {id:33,title:"การตั้งค่า",status1:false,status2:false,status3:false,active:true},
        {id:34,title:"ชื่อเครื่องจักร",status1:true,status2:true,status3:true},
        {id:35,title:"ประเภทเครื่องจักร",status1:false,status2:true,status3:true},
        {id:36,title:"สาเหตุของความสูญเสีย",status1:false,status2:true,status3:true},
        {id:37,title:"ประเภทของความสูญเสีย",status1:false,status2:true,status3:true},
        {id:38,title:"สร้าง account สำหรับผู้ใช้งาน",status1:false,status2:true,status3:true},
        {id:39,title:"กำหนดเวลาพัก",status1:false,status2:true,status3:true},
        {id:40,title:"กำหนดวันทำงาน",status1:false,status2:true,status3:true},
        {id:41,title:"กำหนดวันหยุด",status1:false,status2:true,status3:true},
        {id:42,title:"กำหนดตารางเวลาทำงานรวม",status1:false,status2:true,status3:true},
        {id:43,title:"สาเหตุของเสียจากการผลิต",status1:false,status2:false,status3:true},
    ]
    const table = data2.map(e=>{
        return(
            <tr key={e.id} style={{fontFamily:"Sarabun"}}>
                <td style={{textAlign:'left',backgroundColor:e.active&& '#f2f2f2',pointerEvents:e.active&&'none'}} className="pricing__texttextetx"><h1 className="table__text__55">{e.title}</h1></td>
                <td  style={{textAlign:'center',backgroundColor:e.active&& '#f2f2f2',pointerEvents:e.active&&'none'}} className="pricing__texttextetx">{e.status1 && <h className="table__text__56">✓</h>}</td>
                <td  style={{textAlign:'center',backgroundColor:e.active&& '#f2f2f2',pointerEvents:e.active&&'none'}} className="pricing__texttextetx">{e.status2 && <h className="table__text__56">✓</h>}</td>
                <td  style={{textAlign:'center',backgroundColor:e.active&& '#f2f2f2',pointerEvents:e.active&&'none'}} className="pricing__texttextetx">{e.status3 && <h className="table__text__56">✓</h>}</td>
            </tr>
        )
    })
    return (
        <div className="pricing__main">
            <div className="pricing__header">
                <Header  />
                <HambergaerMenu togglehamberger={setHamshow}/>
            </div>
            <div className="pricing__cotainer__scroll">
                <div className="pricing__header_text">
                    <div>
                        <h1>ชุด Demo SEESET</h1>
                        <h2>
                        คุณสามารถติดต่อเราเพื่อขอ Access key ทางเราจะจัดส่งลิงค์ พร้อมรหัสผ่านไปให้ท่าน<br/> เพื่อให้ท่านทดลองและเรียนรู้ฟีเจอร์และฟังก์ชั่นทั้งหมดที่มีใน SEESET
                        </h2>
                    </div>
                    <Link to="/contact">
                    <button class="button_product button_product1"><Link to="/contact">ติดต่อเรา</Link></button>
                    </Link>
                </div>
                <div className="pricing__table__container">
                    <table id="customers">
                        <tr>
                            <th style={{pointerEvents:'none'}}></th>
                            <th style={{pointerEvents:'none'}}>SEE2S</th>
                            <th style={{pointerEvents:'none'}}>SEE2E</th>
                            <th style={{pointerEvents:'none'}}>SEE2I</th>
                        </tr>
                        {table}
                    </table>
                </div>
                <footer>
                    <h1>***   หมายเหตุ</h1>
                    <h2>ไม่รวมการค้นหาสัญญาณจากเครื่องจักร ลูกค้าต้องเตรียมสัญญาณจากเครื่องจักรไว้ให้ กรณีต้องการให้หาสัญญาณจากเครื่องจักรให้จะมีค่าใช้จ่ายเพิ่มเติม</h2>
                </footer>
            </div>
        </div>
    )
}

export default Pricing
