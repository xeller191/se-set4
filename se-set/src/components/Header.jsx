import React from 'react'
import picture from '../assets/img/SEESETCloudLogo.svg'
import { Link } from "react-router-dom";
import '../assets/scss/header.css'

const Header = () => {
    return (
        <div className="container__main">
            <div className="dummy"/>
            <Link to="/product" className="text-link"><h1>รายละเอียดผลิตภัณฑ์</h1></Link>
            <Link to="/usecase" className="text-link"><h1>กรณีศึกษา</h1></Link>
            <Link to="/home" className="text-link"><img src={picture} alt="" /></Link>
            <Link to="/pricing" className="text-link"><h1>แพ็กเกจของเรา</h1></Link>
            <Link to="/contact" className="text-link"><h1>ติดต่อเรา</h1></Link>
            <div className="dummy"/>
        </div>
    )
}

export default Header
